#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_tests() {
        assert_eq!(2 + 2, 4);
    }

    #[test]
    fn test_zigzag_traverse_3x3() {
        let mat: [[u32; 3]; 3] = [
            [1, 2, 6],
            [3, 5, 7],
            [4, 8, 9]
        ];
        assert_eq!(zigzag_traverse(mat), "123456789");
    }

    #[test]
    fn test_zigzag_traverse_3x4() {
        let mat: [[u32; 3]; 4] = [
            [1, 2, 6],
            [3, 5, 7],
            [4, 8,11],
            [9,10,12]
        ];
        assert_eq!(zigzag_traverse(mat), "123456789101112");
    }

    #[test]
    fn test_zigzag_traverse_4x3() {
        let mat: [[u32; 4]; 3] = [
            [1, 2, 6, 7],
            [3, 5, 8,11],
            [4, 9,10,12]
        ];
        assert_eq!(zigzag_traverse(mat), "123456789101112");
    }
}

struct Point {
    x: usize,
    y: usize,
}

pub fn zigzag_traverse<const M: usize, const N: usize>(mat: [[u32; M]; N] ) -> String {
    let mut out = String::new();
    let mut pos: Point = Point{x: 0, y: 0};
    let mut leftdown: bool = true;
    for _diag in 0..(N+M-1) {
        leftdown = !leftdown;
        loop {
            let cur = mat[pos.y][pos.x];
            out.push_str(&cur.to_string());
            if (leftdown && (pos.x == 0 || pos.y == N - 1)) || (!leftdown && (pos.y == 0 || pos.x == M - 1)) {
                if (leftdown && (pos.x == 0 && pos.y != N-1)) || (!leftdown && pos.x == M-1) {
                    pos.y = pos.y + 1;
                } else {
                    pos.x = pos.x + 1;
                }
                break;
            }
            if leftdown {
                pos.y = pos.y + 1;
                pos.x = pos.x - 1;
            } else {
                pos.y = pos.y - 1;
                pos.x = pos.x + 1;
            }
        }
    }
    return out;
}
